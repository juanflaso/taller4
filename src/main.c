#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "cifrado.h"

#define MAX 50

int main(void) {

    char* msj = malloc(MAX);
    char opt[1];
    int llave;
    char* msjCifrado;
    char* pass=malloc(MAX);

    printf("Ingrese el mensaje: \n");
    fgets(msj, MAX, stdin);
    printf("Su mensaje fue %s\n", msj);

    printf("Tipo de cifrador (1) cíclico (2) autollave: ");
    scanf("%s", opt);
    printf("Entrada: %s\n", opt);

    while(strcmp(opt, "1") != 0 && strcmp(opt, "2") != 0 ) {
        printf("Entrada incorrecta...\nTipo de cifrador (1) cíclico (2) autollave: ");
        scanf("%s", opt);
        printf("Entrada: %s\n", opt);
    }

    if (strcmp(opt, "1") == 0) {
        printf("Cíclico...\n");
        printf("Ingrese la llave: ");
        scanf("%d", &llave);
        msjCifrado = ciclico(msj, llave);
        printf("Su mensaje es: %s\n", msjCifrado);

    } 
    else {
	printf("Ingrese contraseña: \n");
    	scanf("%s", pass);
	//fgets(pass, MAX, stdin);

        msjCifrado= autollave(msj,pass);

	printf("Cifrado: %s\n", msjCifrado);
    }

    return 0;
}
