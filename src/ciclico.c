#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define MIN_CHAR 65
#define MAX_CHAR 90

/* 
* Para esta funcion se utilizan los valores enteros de los char.
* 65 Siendo el mínimo (A)
* 90 SIendo el máximo (B)
* Si la llave numérica es mayor al número de letras delante del carácter que queremos codificar, 
* entonces damos la vuelta y empezamos desde el inicio del alfabeto
*/
char* ciclico(char* msj, int llave) {
    int sumador;
    int sumaChar;
    int stringSize = strlen(msj)+1; 
    char* newString = malloc(stringSize);
    char letra; 

    for (int i = 0; i < strlen(msj); i++) {
        letra = msj[i];

        // Revisamos si el caracter es válido (letra mayúscula)
        if (letra >= 65 && letra <= 90) { 
            sumador = llave%26; 
            sumaChar = letra + sumador;
            
            if (sumaChar > MAX_CHAR) { 
                /*
                * Si la suma es mayor que el MAX_CHAR (90) se le resta dicho valor.
                * A este valor se le resta 1 para simular el "salto" que se da al ir de MAX_CHAR a MIN_CHAR 
                * El resultado se le suma a 65 para obtener el valor final de la operación
                */
                sumaChar = (65 + ((sumaChar-MAX_CHAR)-1));            
            }
            newString[i] = sumaChar; 
        }
        // si no es válido se reemplaza por espacio
        else {
            newString[i] = 32;
        }
    }
    return newString;
}