#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MIN_CHAR 65
#define MAX_CHAR 90

char TABULA_RECTA[26][26];



/*
En esta funcion se necesita una contraseña, se anexa al texto a cifrar
y luego alineamos el texto a cifrar con el nuevo string generado. \n
*/

char* autollave(char* msj , char* pass){	
	char passw[strlen(msj)+strlen(pass)];

	printf("%s",msj);
	strcpy(passw,pass);
	strcat(passw,msj);
	int stringSize = strlen(msj); 
    	char* newString = malloc(stringSize);	
	char letra , letra2;


	int i , j=0;
	int sumaChar;

	for (i=0 ; i<strlen(msj)-1 ; i++){
		letra=msj[i];
		letra2=passw[j];
		
		if (letra2 ==' ') {
			j++;
      		}
		if (letra==' ') { 
			newString[i] = 32;
		    	j--;			
       		}
        	else {
		    	sumaChar = letra + letra2 - MIN_CHAR;
		    	if (sumaChar > MAX_CHAR) { 
		      
		        sumaChar = (65 + ((sumaChar-MAX_CHAR)-1));          
		    	}
		   	newString[i] = sumaChar; 
			
        	}
		j++; 
			
	}

	return newString;	
}


//En esta funcion se llena la TABULA_RECTA
void llenarTabla(){
//'A'=65 .... 'Z'=90
	int i;
	for(i=0; i<26 ; i++){
		int j;
		int vuelta=0; //variable vuelta indica cuando ya cayo en el caracter 'Z'
		for(j=0 ; j<26 ; j++){
			
			if(90 < 65+j+i && i>0){
				TABULA_RECTA[i][j]=65+vuelta; //aqui comienza nuevamente a llenar del caracter 'A'
				vuelta++;
			
			}
			
			else
			TABULA_RECTA[i][j]=65+i+j;
			
		}
	}
	
	
}

