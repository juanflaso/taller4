cifrado: ciclico.o autollave.o main.o 
	gcc obj/ciclico.o obj/autollave.o obj/main.o -o bin/cifrado 

ciclico.o: src/ciclico.c  
	gcc -Wall -c -I include/ src/ciclico.c -o obj/ciclico.o

autollave.o: src/autollave.c  
	gcc -Wall -c -I include/ src/autollave.c -o obj/autollave.o

main.o: src/main.c  
	gcc -Wall -c -I include/ src/main.c -o obj/main.o

